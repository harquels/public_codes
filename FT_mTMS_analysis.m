function [] = FT_mTMS_analysis(vStep, viSubj, viSession, sGavg, sGavg_ind)
% CNRS UMR 5105 - Laboratoire de Psychologie et Neurocognition
% S. Harquel & B. Passera
% 2019-2021


% =================== PLOT ==============================================
vZLIM = [-6 4];
vPlotTime = [-0.04 : 0.02 : 0.42]; %en sec
bZSCORE = 1; 

% ==================PARAMETERS RGRESSION ================================
PE_IND = 5;

 % define which TEP to be regressed in all single trials (1=M1_1, 2=M1_2, etc.)
% !!! define PE_IND = 0 for regressing each TEP within its own trials (= PAIRED intensities)

% =================== DEFINE =============================================
% sujets
ceSUBJ = {'TIXMA_p01','02PONEL','03BRULI','04CLEOL','05MACLA','06FAUAL','07JOSLA','08ELBSA','09ENTLE'...
    '10HEULU','11DIDME','12ROGEL','13LAFRE','14BLASO','15DELAD','16ROUAL','17DUVEL','18CHATH','19DINRE','20GERPA'};

% Session 
ceSESSION = {'M1_1','M1_2','M1_3','S1_1','S1_2','S1_3','PM_1','PM_2' ...
    'PM_3','SHAM'}; 
   
ceCONDSESS = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};

viCond = viSession;
ceCOND = ceSESSION;

% % Definition des channels (ROIs scalp) par condition 
ceCHANCOND = { { 'C2' 'FCC2h' 'FCC4h' 'CCP2h' 'CCP4h'} {'C2' 'FC2' 'C4' 'FCC4h' 'FCC6h'} {'FCC6h' 'FC6' 'FC4' 'C4' 'C6'} ... % M1
    { 'C2' 'CP2' 'C4' 'CP4' 'CCP4h'} {'CCP4h' 'CCP6h' 'C4' 'FCC6h' 'C6'} {'CCP6h' 'FC6' 'C4' 'FCC6h' 'C6'} ... % S1
    {'C2' 'FC2' 'FCC2h' 'FCC4h' 'FFC2h' } {'FC2' 'FCC2h' 'FC4' 'FFC2h' 'FFC2h'} {'FFC4h' 'FC4' 'FC2' 'FCC6h'} ... %PM
    {'Fp2' 'AF8' 'AF4' 'AFF6h' 'F6'}}; % sham


ceTRIG = {'R128'}; %trig a analyser
cTRIGTYPE = 'Response'; %type de trig a analyser
cTRIALFUN = 'trialfun_robexci'; %fonction utilisee lors de definetrial

ceCONDTRIG = {[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2]...
    ,[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2],[1 2]};

% Channels to process
ceEEGCHAN = {'all'};
cSTDCHANFILE = 'BrainAmpLabel_128';
ceROICHAN = {}; %chan d'interets

% Layout et voisins
cLAYOUTFILE = 'BrainAmpLayout_128.mat';
cNEIGFILE = 'BrainAmpNeighbours_128.mat';

% Prestim et poststim (en s) - étape 1
PRESTIM = 1; % voir avec bsl
POSTSTIM = 1;

% Debut et fin baseline (en s) - étape 3 % par rapport à 30/31
BSLBEG = -0.2;
BSLEND = -0.005;

% Filtrage (passe haut, passe bas) - étape 3
HPFREQ = 1;
LPFREQ = 80;

% Keep trials ou pas pour stats indivi (ATTENTION MEMOIRE)
bKEEPTRIALS = 0;

% Sauvegarde
%data
bSAVEDATA=1;
%fig
bSAVEFIG=1;
bVISIBLE =1;
cPICFOLDER = 'pic/recrut';
cPICNAME = 'OccSup';

cGAVGNAME = 'GAVG_Z';
cPREPROCSUFFIX = ['_ICARog_BST'];
cSUFFIX=['_AVG_' num2str(HPFREQ) '-' num2str(LPFREQ) '_' num2str(BSLBEG) cPREPROCSUFFIX];


% =============== STep  1 -- DEFINE TRIALS AND PREPROCESSING =====================
% ================================================================================
if ~isempty(find(vStep==1))
    
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        sDir = dir('*.eeg'); %liste fichiers EEG (* = toute chaine possible)
        
        bBadChanDet = 1;
        
        try
            load([cName '_' 'preproc.mat']);
        catch
            warning(['No previous preproc file found']);
        end
        
        % ================ Channels artefactees  =====================
        if bBadChanDet,
            for iSess = viSession
                
                %nom du fichier a trouver
                cFileName = ceSESSION{iSess}; 
                
                for iFile = 1 : numel(sDir)
                    Pos = strfind(sDir(iFile).name, [cName]); % cherche la chaine 'cName' dans tous les fichiers
                    if ~isempty(Pos),
                        Pos2 = strfind(sDir(iFile).name, cFileName);
                        if ~isempty(Pos2), break;  end %si Pos n'est pas vide, chaine trouvee, on sort de la boucle
                    end
                end              
                if isempty(Pos) || isempty(Pos2),
                    errordlg(['No file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                    continue
                end

                sCfg = [];
                sCfg.trialfun = cTRIALFUN;
                sCfg.dataset = sDir(iFile).name; % Fichier de donnees trouvees
                sCfg.trialdef.eventtype = cTRIGTYPE; % Type d'evenement
                sCfg.trialdef.eventvalue = ceTRIG;
                sCfg.trialdef.prestim = PRESTIM; %limite fentre pre stim (en s)
                sCfg.trialdef.poststim = POSTSTIM; %limite fentre post stim (en s)
                sCfg = ft_definetrial(sCfg);
                
                sCfg.channel = ceEEGCHAN;
                sCfg.refchannel = 'all';
                sCfg.demean = 'yes';
                sCfg.lpfilter = 'yes';
                sCfg.lpfreq = LPFREQ;
                sData_chan = ft_preprocessing(sCfg);
                

                sCfg          = [];
                sCfg.method   = 'channel';
                sCfg.channel = 'all';
                sCfg.alim    = 100; % amplitude
                sData_chan      = ft_rejectvisual(sCfg,sData_chan);
                
                ceCHAN = sData_chan.label;
                ceBADCHAN = setxor(sData_chan.label,sData_chan.cfg.channel);
                
                save([cName '_' cFileName '_chan'],'ceCHAN','ceBADCHAN');

                
            end
        end
        
        cd('../');
        
    end
end




% =============== STep  2 -- PREPROCESSING & ICA Rogasch NeuoImage Method========
% ================================================================================
if ~isempty(find(vStep==2))
    
    
    %         hWt = waitbar(0,'');
    
    load(cSTDCHANFILE);
    ceStdLabel(find(strcmp(ceStdLabel,'EOG')))=[];
    load(cNEIGFILE);
    
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        
        sDir = dir('*.eeg'); %liste fichiers XEEG (* = toute chaine possible)
        
        
        for iSess = viSession
            
            % looking for the corresponding eeg file
            for iFile = 1 : numel(sDir)
                Pos = strfind(sDir(iFile).name, [cName]); % cherche la chaine 'cName' dans tous les fichiers
                if ~isempty(Pos),
                    Pos2 = strfind(sDir(iFile).name, ceSESSION{iSess});
                    if ~isempty(Pos2), break;  end %si Pos n'est pas vide, chaine trouvee, on sort de la boucle
                end
            end
            
            if isempty(Pos) || isempty(Pos2),
                errordlg(['No file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                continue
            end
            
            % looking for the corresponding chan files
            try
                load([cName '_' ceSESSION{iSess} '_chan']);
            catch
                disp('No chan file found ! Assuming no bad chan');
                ceCHAN = ceStdLabel;
                ceBADCHAN = {};
            end
            
            
            
            % =============== Definition et preproc =================
            %  Definition des trials
            sCfg = [];
            sCfg.dataset = sDir(iFile).name; % Fichier de données trouvé
            sCfg.trialdef.eventtype = cTRIGTYPE; % Type d'evenement
            sCfg.trialdef.eventvalue = ceTRIG;
            sCfg.trialdef.prestim = PRESTIM; %limite fentre pre stim (en s)
            sCfg.trialdef.poststim = POSTSTIM; %limite fentre post stim (en s)
            if iSubj == 9 || iSubj == 13, sCfg.trialfun = 'trialfun_tmseeg_NOTRIG';    else   sCfg.trialfun = cTRIALFUN; end
            sCfg = ft_definetrial(sCfg);
            ceTrl{iSess,1} = sCfg.trl;
            
            if iSubj == 9 || iSubj == 13 % pour LAFRE et ENTLE
                

                sCfg.method = 'detect';                
                sCfg.dataset = sDir(iFile).name;
                sCfg.prestim = .005;     % First time-point of range to exclude
                sCfg.poststim = .015;     % Last time-point of range to exclude %
                sCfg.artfctdef.tms.channel = ceCHANCOND{iSess};
                sCfg_artfct = ft_artifact_tms(sCfg);     % Detect TMS artifacts
                % ========= !! Replace fieldtrip's artefact by the real ones found within trialfun function !! ==========
                sCfg_artfct.artfctdef.tms.artifact = ceTrl{iSess}(:,end-1:end);
                ceTrl{iSess} = ceTrl{iSess}(:,1:end-2); % remove them after
            
            else
                
            
                sCfg = [];
                sCfg.method = 'marker';
                sCfg.dataset = sDir(iFile).name;
                sCfg.prestim = .005;     % First time-point of range to exclude
                sCfg.poststim = .015;     % Last time-point of range to exclude %
                sCfg.trialdef.eventtype = cTRIGTYPE;
                sCfg.trialdef.eventvalue = ceTRIG ;
                sCfg_artfct = ft_artifact_tms(sCfg);     % Detect TMS artifacts
            
            end
            
            % Rejet des artefacts TMS
            sCfg_artfct.artfctdef.reject = 'partial'; % Can also be 'complete', or 'nan';
            sCfg_artfct.trl = ceTrl{iSess,1}; % We supply ft_rejectartifact with the original trial structure so it knows where to look for artifacts.
            sCfg_artfct.artfctdef.minaccepttim = 0.01; % This specifies the minimumm size of resulting trials. You have to set this, the default is too large for thre present data, resulting in small artifact-free segments being rejected as well.
            sCfg = ft_rejectartifact(sCfg_artfct); % Reject trials partially
            
            sData_tmp  = ft_preprocessing(sCfg);
           
            
            sCfg = [];
            sCfg.channel     = ceCHAN;
            sCfg.reref       = 'yes';
            sCfg.refchannel  = {'all'};
            sData_tmp  = ft_preprocessing(sCfg,sData_tmp);
            
            
            
            % =============== 1st round ICA ===============================
            sCfg = [];
            sCfg.demean = 'yes';
            sCfg.method = 'fastica';        % FieldTrip supports multiple ways to perform ICA, 'fastica' is one of them.
            sCfg.fastica.approach = 'symm'; % All components will be estimated simultaneously.
            sCfg.fastica.g = 'tanh';
            sComp = ft_componentanalysis(sCfg, sData_tmp);
            
            
            % Moyennage des comp sur tous les trials
            sCfg = [];
            sCfg.vartrllength  = 2; % This is necessary as our trials are in fact segments of our original trials. This option tells the function to reconstruct the original trials based on the sample-information stored in the data
            sComp_avg = ft_timelockanalysis(sCfg, sComp);
            sComp_avg.time = {sComp_avg.time};
            sComp_avg.trial = {sComp_avg.avg};
            sComp_avg.fsample = sComp.fsample;
            
            
            
            % =========== Rejet auto de la comp musculaire  ====================
            
            % on recommece sans demean (demean sert a simplifier l'ICA (option par defaut) ...
            % mais enleve la moyenne par TRIALS (donc discotinuite entre trial avant
            % artefact TMS et apres))
            sCfg          = [];
            sCfg.demean   = 'no'; % This has to be explicitly stated as the default is to demean.
            sCfg.unmixing = sComp.unmixing; % Supply the matrix necessay to 'unmix' the channel-series data into components
            sCfg.topolabel = sComp.topolabel; % Supply the original channel label information
            sComp = ft_componentanalysis(sCfg, sData_tmp);
            
            % Comp musculaire = comp la plus grande
            [iComp, j] = find(abs(sComp_avg.avg) == max(abs(sComp_avg.avg(:))));
            
            sCfg            = [];
            sCfg.component = iComp;
            sCfg.demean     = 'no';
            sData_eeg = ft_rejectcomponent(sCfg, sComp);
           
            
            % =========== Interpolation ================
            % Apply original structure to segmented data, gaps will be filled with nans
            sCfg     = [];
            sCfg.trl = ceTrl{iSess,1};
            sData_eeg = ft_redefinetrial(sCfg, sData_eeg); % Restructure cleaned data
            
            
            % Interpolate nans using AR model
            for iTrl = 1 : numel(sData_eeg.trial)
                disp(['Interpolation for trial n ' num2str(iTrl)]);
                for iChan = 1 : size(sData_eeg.trial{iTrl},1)
                    sData_eeg.trial{iTrl}(iChan,:) = fillgaps(sData_eeg.trial{iTrl}(iChan,:),50);
                end
            end
            
            
            % Resampling 
            sCfg = [];
            sCfg.resamplefs = 1000; % Fs = 1000 Hz
            sCfg.detrend = 'no';
            sData_eeg  = ft_resampledata(sCfg,sData_eeg);
            
            
            % =========== Filtrage 1-80 Hz ====================
            for iTrl = 1 : numel(sData_eeg.trial), sData_eeg.trial{iTrl} = ft_preproc_highpassfilter(sData_eeg.trial{iTrl},sData_eeg.fsample,1); end
            for iTrl = 1 : numel(sData_eeg.trial), sData_eeg.trial{iTrl} = ft_preproc_lowpassfilter(sData_eeg.trial{iTrl},sData_eeg.fsample,80); end
            
            
            save([cName  '_' ceSESSION{iSess} '_cleanSTP2'],'sData_eeg','sComp','-v7.3');
            
        end
        cd ..;
    end
end


% =============== STep  3 -- Rejet trials artefacts - Rogasch NeuoImage Method========
% =================================================================================

if ~isempty(find(vStep==3))
   
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        for iSess = viSession
            
            try
                load([cName  '_' ceSESSION{iSess} '_cleanSTP2']);
            catch
                errordlg(['No cleanTMP file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                continue
            end

            % ================ Rejection Manuelle =========================
            sCfg =[];
            sCfg.viewmode = 'vertical';
            sCfg.channel = 'all';
            sCfg_art = ft_databrowser(sCfg,sData_eeg);
            sCfg_art.artfctdef.reject = 'complete';
            sData_eeg = ft_rejectartifact(sCfg_art,sData_eeg);
            
            
            % garde trace du nb de trials gardes
            mMetaData(iSess,4) = numel(sData_eeg.trial);

            save([cName  '_' ceSESSION{iSess} '_cleanSTP3'],'sData_eeg','mMetaData','-v7.3');
        end
        cd ..;
    end
end

% =============== Step  4 -- ICA 2nd round - Rogasch NeuoImage Method========
% =================================================================================

if ~isempty(find(vStep==4))
    
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        for iSess = viSession
            
            try
                load([cName  '_' ceSESSION{iSess} '_cleanSTP3']);
            catch
                errordlg(['No cleanTMP file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                continue
            end
            
            % =============== 2nd round ICA ===============================
            sCfg = [];
            sCfg.demean = 'no';
            sCfg.method = 'fastica';        % FieldTrip supports multiple ways to perform ICA, 'fastica' is one of them.
            sCfg.fastica.approach = 'symm'; % All components will be estimated simultaneously.
            sCfg.fastica.g = 'tanh';
            sCfg.numcomponent = 'all';
            sComp = ft_componentanalysis(sCfg, sData_eeg);
            
            save([cName  '_' ceSESSION{iSess} '_comp_ICA_2nd'],'sComp','mMetaData','-v7.3');
        end
        cd ..;
    end
end


% =============== Step  5 -- ICA 2nd round - Rogasch NeuoImage Method========
% =================================================================================
if ~isempty(find(vStep==5))
    
    
    load(cSTDCHANFILE);
    ceStdLabel(find(strcmp(ceStdLabel,'EOG')))={};
    load(cNEIGFILE);
    
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        for iSess = viSession
            
            % looking for the corresponding chan files
            %nom du fichier a trouver
            cFileName = ceSESSION{iSess};
            try
                load([cName '_' cFileName '_chan']);
            catch
                disp('No chan file found ! Assuming no bad chan');
                ceCHAN = ceStdLabel;
                ceBADCHAN = {};
            end
                 
            try
                load([cName  '_' ceSESSION{iSess} '_comp_ICA_2nd']);
            catch
                errordlg(['No file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                continue
            end
            
            
            % ================ VISUALISATION CLASSIQUE ====================
            % Moyennage des comp sur tous les trials
            sCfg = [];
            sCfg.vartrllength  = 2; % This is necessary as our trials are in fact segments of our original trials. This option tells the function to reconstruct the original trials based on the sample-information stored in the data
            sComp_avg = ft_timelockanalysis(sCfg, sComp);
            sComp_avg.time = {sComp_avg.time};
            sComp_avg.trial = {sComp_avg.avg};
            sComp_avg.sampleinfo = sComp.sampleinfo(2,:);
            sComp_avg.fsample = sComp.fsample;
            sComp_avg.topolabel = sComp.topolabel;
            
            % affichage moyenne des comps
            sCfg = [];
            sCfg.layout    = cLAYOUTFILE;
            sCfg.viewmode  = 'component';
            sCfg.compscale = 'local';
            sCfg.blocksize = 2;
            ft_databrowser(sCfg, sComp_avg);
            colormap jet;
            set(gcf,'Position',[20    42   801   794]); % position laptop
            
            % affichage trial par trial :
            ft_databrowser(sCfg, sComp);
            set(gcf,'Position',[769    35   810   796]); % position laptop
            colormap jet;
            
            
            % =========== Auto ================
            
            % 1. Detection automatique des comp artefactees (> 5 zscore)
            Ncomp = size(sComp_avg.avg,1);
            vTmp = sComp_avg.avg(:,nearest(sComp_avg.time{1},-0.6):nearest(sComp_avg.time{1},0.0));
            
            C_z = 5;
            
            viComp = [];
            for iComp = 1:Ncomp
                Std_Bsl = nanstd(vTmp(iComp,:));
                Mean_Bsl = nanmean(abs(vTmp(iComp,:)));
                if ~isempty( find(abs(sComp_avg.avg(iComp,1000:1600)) > (C_z*Std_Bsl + Mean_Bsl))), viComp = [viComp iComp]; end
            end
            
            disp(['Z threshold is ' num2str(C_z)]);
            
            disp(['======> Composantes artefactees pour ' ceSUBJ{iSubj} ' et ' ceSESSION{iSess} ' : ' num2str(viComp)]);
            mMetaData(iSess,1) = numel(viComp);
            
            
            % =========== Affichage par paquet de trials type eeglab ======
            for iTrl = 1 : numel(sComp.trial)
                for iComp = 1 : size(sComp.topo, 2)
                    mAllTrl(iTrl,:,iComp) = sComp.trial{iTrl}(iComp,:);
                end
            end
            
            Plot_Comp(mAllTrl)

            
            % =========== Manuel =======================
            
            % choix comp
            viComp_Man = input('Comp a enlever : ');
            mMetaData(iSess,3) = numel(viComp_Man);
            
            % Rejet de toutes les comp
            viRmComp        = viComp_Man;
            sCfg            = [];
            sCfg.component = viRmComp;
            sCfg.demean     = 'no';
            sData_clean = ft_rejectcomponent(sCfg, sComp);
            
            
            % =========== reconstruction des chan manquantes ==============
            while numel(setxor(ceStdLabel,sData_clean.label)) > 0
                
                ceBADCHAN = setxor(ceStdLabel,sData_clean.label);
                
                sCfg = [];
                sCfg.neighbours = sNeighbours.neighbours;
                sCfg.missingchannel = ceBADCHAN;
                sCfg.layout = cLAYOUTFILE;
                sCfg.method = 'average';
                [sInterp] = ft_channelrepair(sCfg, sData_clean);
                
                sData_clean.trial = sInterp.trial;
                sData_clean.time = sInterp.time;
                sData_clean.label = sInterp.label;
                
            end
            
            % on remet les chan dans le bon ordre
            sData_clean = FT_robexci_tri_cellule_chan(sData_clean,ceStdLabel,0,'Data');
            
            if bSAVEDATA, save([cName  '_' ceSESSION{iSess} cPREPROCSUFFIX],'sData_clean','mMetaData'); end
            
        end
        
        cd('../');
        
    end
end





% =============== Step  6 ======== Assemblage toutes cond =================
% ================================================================================

if ~isempty(find(vStep==6))
    
    
    
    for iSubj = viSubj
        
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        for iSess = viSession
            try
                disp(['Loading session ' num2str(iSess)]);
                load([cName  '_' ceSESSION{iSess} cPREPROCSUFFIX]);
            catch
                errordlg(['No file found for '  cName ' and session ' ceSESSION{iSess} '...']);
                sData_clean = [];
            end
            
            if ~isempty(sData_clean)

                sData(iSess).trial = sData_clean.trial;
                sData(iSess).time = sData_clean.time;
                sData(iSess).label = sData_clean.label;
                sData(iSess).fsample = sData_clean.fsample;
                sData(iSess).trialinfo = sData_clean.trialinfo;
                sData(iSess).sampleinfo = sData_clean.sampleinfo;
                sData(iSess).cfg = sData_clean.cfg;
        
            else
                sData(iSess) = sData(iSess-1);
                sData(iSess).trial = [];
            end
        end
        
        clear sData_clean;
        
        
        sData_clean(viSession) = sData(viSession);
        if bSAVEDATA, save([cName '_' 'clean' cPREPROCSUFFIX '.mat'],'sData_clean','mMetaData'); end
        
        clear sData;
        
        cd('../');
        
    end
end



%
% ========================= Step 7 CALCULS PE =====================================
%==================================================================================
%
if ~isempty(find(vStep==7))
    
    hWaitbar = waitbar(0,'Please wait');
    
    
    for iSubj = viSubj
        
        % Nom du sujet
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        
        try
            load([cName '_' 'clean' cPREPROCSUFFIX '.mat']);
            disp(['Traitement de ' cName]);
        catch
            disp([cName ' : pas de fichier clean ou preproc trouve - sujet suivant']);
            cd ..
            continue;
        end
        
        try
            load([cName cSUFFIX '.mat']);
        catch
            warning('No previous AVG file found');
        end
        
        
        %==================average by condition============================
        
        for iCond = viCond
            
            
            if ceCONDSESS{iCond}(1)==0,
                warning(['Pas de trials trouves pour ' cName ' dans la condition ' ceCOND{iCond} '. Condition suivante...']);
                if exist('sTmp'), sTmp(iCond).avg = []; end;
                continue;
            end
            
            
            % donnees a moyener
            sData2avg = sData_clean(ceCONDSESS{iCond}(1));
            
            disp(['Cond n° ' num2str(iCond)]);
            
            % si elle sont sur plusieures sessions, les regrouper avant
            if numel(ceCONDSESS{iCond}) > 1
                for iSess = ceCONDSESS{iCond}(2:end)
                    sData_tmp = sData_clean(iSess);
                    sData2avg.trial = [sData2avg.trial sData_tmp.trial];
                    sData2avg.time = [sData2avg.time sData_tmp.time];
                    sData2avg.trialinfo = [sData2avg.trialinfo ; sData_tmp.trialinfo];
                    sData2avg.sampleinfo = [sData2avg.sampleinfo ; sData_tmp.sampleinfo];
                end
                clear sData_tmp
            end
            
            if isempty(sData2avg.trial),
                warning(['Pas de trials trouves pour ' cName ' dans la condition ' ceCOND{iCond} '. Condition suivante...']);
                if exist('sTmp'), sTmp(iCond).avg = []; end;
                continue;
            end
            
            
            % 1. AVG par CONDITIONS, selon SESSIONS et TRIGG
            sCfg = [];
            viTrl = [];
            for iTrigStim = 1 : numel(ceCONDTRIG{iCond}), viTrl = [viTrl find(sData2avg.trialinfo == ceCONDTRIG{iCond}(iTrigStim))']; end
            sCfg.trials = viTrl;
            sCfg.keeptrials = 'no';
            
            % basline definition
            sCfg.preproc.demean = 'yes';
            sCfg.preproc.baselinewindow = [BSLBEG BSLEND];
            sCfg.channel = 'all';
            sCfg.trials = 'all';
            sDumb = ft_timelockanalysis(sCfg,sData2avg);
            
            % structure copy
            sTmp(iCond).time = sDumb.time;
            sTmp(iCond).label = sDumb.label;
            sTmp(iCond).avg = sDumb.avg;
            sTmp(iCond).var = sDumb.var;
            sTmp(iCond).dof = sDumb.dof;
            sTmp(iCond).dimord = sDumb.dimord;
            sTmp(iCond).cfg = sDumb.cfg;
             
            
            % on trials
            if bKEEPTRIALS
                sDumb = [];
                sCfg.keeptrials = 'yes';
                sDumb = ft_timelockanalysis(sCfg,sData2avg);
                
                % structure copy
                sTmp(iCond).trial = sDumb.trial;
                
                sCfg = [];
                sCfg.baseline = [BSLBEG BSLEND];
                sCfg.parameter = 'trial';
                sData_bsl = ft_timelockbaseline(sCfg,sTmp(iCond));
                sTmp(iCond).trial = sData_bsl.trial;
            end
            
            
            % ==================  ZSCORE ====================
            viTime = [nearest(sTmp(iCond).time, BSLBEG): nearest(sTmp(iCond).time, BSLEND)];
            vMean = mean(sTmp(iCond).avg(:,viTime),2);
            vStd = std(sTmp(iCond).avg(:,viTime),[],2); % zscore
            mMean = repmat(vMean,1,size(sTmp(iCond).avg,2));
            mStd = repmat(vStd,1,size(sTmp(iCond).avg,2));
            
            sTmp(iCond).zscore = (sTmp(iCond).avg - mMean) ./ mStd;
            
            % on trials
            if bKEEPTRIALS
                for iTrl = 1 : size(sTmp(iCond).trial,1)
                    sTmp(iCond).trial(iTrl,:,:) = (squeeze(sTmp(iCond).trial(iTrl,:,:)) - mMean) ./ mStd;
                end
            end            
        end

        sAvg(viCond) = sTmp(viCond);
        if bSAVEDATA, save([cName cSUFFIX '.mat'],'sAvg'); end
        waitbar(iSubj/(numel(viSubj)),hWaitbar);
        
        cd('../');
        clear sTmp
        clear sAvg
        
    end
    close(hWaitbar);
    
    
end


% ========================== STEP 8 -- GRAND AVG ==================================
%==================================================================================

if ~isempty(find(vStep==8))
    
    try load([cGAVGNAME cSUFFIX '.mat']); catch warning('No GAVG file'); end
    
    cAllSubj = [];
    
    for iSubj = viSubj
        
        cName =  ceSUBJ{iSubj};
        
        cd(cName);
        
        load([cName cSUFFIX '.mat']);
        ceAllAvg{iSubj} = sAvg; % cell contenant tous les avg
        cAllSubj = [cAllSubj 'ceAllAvg{' num2str(iSubj) '}(iCond),'];
        
        cd('../');
        
    end
    
    for iCond = viCond
        
        cSubj = cAllSubj;
        
        % elimination des sujets ne possedant pas la condition
        for iSubj = viSubj
            if isempty(ceAllAvg{iSubj}(iCond).avg),
                eval(['iRm = findstr(cSubj,''ceAllAvg{' num2str(iSubj) '}(iCond)'');']);
                if iSubj < 10, cSubj(iRm:iRm+18) = []; else cSubj(iRm:iRm+19) = []; end
            end
        end
        
        sCfg = [];
        if bZSCORE, sCfg.parameter = 'zscore'; end
        sCfg.keepindividual = 'no';
        eval([ 'sGavg(iCond) = ft_timelockgrandaverage(sCfg,' cSubj(1:end-1) ');' ]);
        sCfg.keepindividual = 'yes';
        eval([ 'sGavg_ind(iCond) = ft_timelockgrandaverage(sCfg,' cSubj(1:end-1) ');' ]);
        sGavg(iCond).avg = single(sGavg(iCond).avg);
        sGavg_ind(iCond).individual = single(sGavg_ind(iCond).individual);
        
    end
        
    
    if bSAVEDATA, save([cGAVGNAME cSUFFIX '.mat'],'sGavg','sGavg_ind'); end

end



% ================== ANALYSES COMPLEMENTAIRES =============================

% =============== Régression linéaire single trial ========================

if ~isempty(find(vStep==5001))
    
    % matrice des betas de la regression : nb cond * 130 * n sujets
    mBeta = zeros(max(viCond),130,numel(ceSUBJ)) + NaN;
    mPval = mBeta; %p values correspondantes
    mTval = mBeta; %t values
    
    for iSubj = viSubj
        
        disp(['Procesing subj n' num2str(iSubj)]);
           
        cName =  ceSUBJ{iSubj};
        cd(cName);
        
        load([cName cSUFFIX '.mat']);
        
        %TOI en sample
        viTime = [nearest(sAvg(1).time,0.015) : nearest(sAvg(1).time,0.08)]; % 15 ms à 80 ms
        
        
        for iCond=viCond
            
            disp(['Procesing subj n' num2str(iSubj) ' and cond ' num2str(iCond)]);
            
            if isempty(sAvg(iCond).avg), continue; end
            
            %ROI
            
            % Quel TEP utiliser ?
            if PE_IND == 0      % TEP-trial paired
                viChan = Acticap_Chan2Num(ceCHANCOND{iCond},sAvg(iCond).label);
                vPE = mean(sAvg(iCond).avg(viChan,viTime),1)';
                 % name of regression's datafile
                cREGNAME = 'PAIRED';
            else                % unique TEP for all 
                viChan = Acticap_Chan2Num(ceCHANCOND{PE_IND},sAvg(PE_IND).label);
                vPE = mean(sAvg(PE_IND).avg(viChan,viTime),1)';
                cREGNAME = ceCOND{PE_IND};
            end

            
            % estimation tous les trials
            for iTrl = 1 : size(sAvg(iCond).trial,1)

                % y(t)
                vPE_trl = squeeze(mean(sAvg(iCond).trial(iTrl,viChan,viTime),2));

                %Modele
                mdl = LinearModel.fit(vPE,vPE_trl);
                mBeta(iCond,iTrl,iSubj) = mdl.Coefficients.Estimate(2);
                mPval(iCond,iTrl,iSubj) = mdl.Coefficients.pValue(2);
                mTval(iCond,iTrl,iSubj) = mdl.Coefficients.tStat(2);
                  
            end
        end
        
        cd ..;
        
    end
    
   
    save(['Regr_SingleTrl_' cREGNAME '.mat'],'mBeta','mPval','mTval');
    
end


